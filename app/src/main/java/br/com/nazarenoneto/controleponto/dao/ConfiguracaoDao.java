package br.com.nazarenoneto.controleponto.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import br.com.nazarenoneto.controleponto.model.Configuracao;

/**
 * Created by nazareno on 07/04/2016.
 */
public class ConfiguracaoDao extends BaseDaoImpl<Configuracao, Integer> {

    public ConfiguracaoDao(ConnectionSource connectionSource, Class<Configuracao> dataClass) throws SQLException {
        super(connectionSource, dataClass);
        setConnectionSource(connectionSource);
        initialize();
    }
}
