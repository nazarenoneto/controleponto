package br.com.nazarenoneto.controleponto.util;

import android.content.Context;
import android.widget.Toast;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

/**
 * Created by nazareno on 13/05/2016.
 */
@EBean
public class ToastUtil {

    @RootContext
    Context context;

    @UiThread
    public void exibeMensagem(String mensagem, boolean duracaoLonga) {
        Toast.makeText(context, mensagem, duracaoLonga ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }
}
