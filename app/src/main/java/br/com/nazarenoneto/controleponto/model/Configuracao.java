package br.com.nazarenoneto.controleponto.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by nazareno on 07/04/2016.
 */

@DatabaseTable(tableName = Configuracao.TABLE_NAME)
public class Configuracao implements Serializable {

    public static final String TABLE_NAME = "CONFIGURACAO";
    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_CODIGO = "CODIGO";
    public static final String COLUMN_DATA_CAD = "DATA_CAD";
    public static final String COLUMN_DATA_MOD = "DATA_MOD";
    public static final String COLUMN_NOME = "NOME";
    public static final String COLUMN_EMAIL = "EMAIL";
    public static final String COLUMN_QUANTIDADE_BATIDAS = "QUANTIDADE_BATIDAS";

    public static final String INFO = Configuracao.class.getSimpleName();

    @DatabaseField(id = true, columnName = Configuracao.COLUMN__ID)
    private int _id;
    @DatabaseField(columnName = Configuracao.COLUMN_CODIGO)
    private String codigo;
    @DatabaseField(columnName = Configuracao.COLUMN_DATA_CAD)
    private Date dataCad;
    @DatabaseField(columnName = Configuracao.COLUMN_DATA_MOD)
    private Date dataMod;
    @DatabaseField(columnName = Configuracao.COLUMN_NOME)
    private String nome;
    @DatabaseField(columnName = Configuracao.COLUMN_EMAIL)
    private String email;
    @DatabaseField(columnName = Configuracao.COLUMN_QUANTIDADE_BATIDAS)
    private int quantidadeBatidas;

    public Configuracao() {
        codigo = UUID.randomUUID().toString();
        dataCad = new Date(System.currentTimeMillis());
        dataMod = new Date(System.currentTimeMillis());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Configuracao that = (Configuracao) o;

        return codigo != null ? codigo.equals(that.codigo) : that.codigo == null;

    }

    @Override
    public int hashCode() {
        return codigo != null ? codigo.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Configuracao{" +
                "_id=" + _id +
                ", codigo='" + codigo + '\'' +
                ", dataCad=" + dataCad +
                ", dataMod=" + dataMod +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", quantidadeBatidas='" + quantidadeBatidas + '\'' +
                '}';
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getDataCad() {
        return dataCad;
    }

    public void setDataCad(Date dataCad) {
        this.dataCad = dataCad;
    }

    public Date getDataMod() {
        return dataMod;
    }

    public void setDataMod(Date dataMod) {
        this.dataMod = dataMod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getQuantidadeBatidas() {
        return quantidadeBatidas;
    }

    public void setQuantidadeBatidas(int quantidadeBatidas) {
        this.quantidadeBatidas = quantidadeBatidas;
    }
}
