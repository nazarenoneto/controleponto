package br.com.nazarenoneto.controleponto.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import br.com.nazarenoneto.controleponto.dao.ConfiguracaoDao;
import br.com.nazarenoneto.controleponto.database.DatabaseHelper;
import br.com.nazarenoneto.controleponto.model.Configuracao;

/**
 * Created by nazareno on 07/04/2016.
 */
@EBean
public class ConfiguracaoService {
    private Configuracao configuracao;
    @OrmLiteDao(helper = DatabaseHelper.class)
    //ConfiguracaoDao configuracaoDao;
            Dao<Configuracao, Integer> configuracaoDao;
    private DatabaseHelper databaseHelper;
    @RootContext
    Context context;

//    private ConfiguracaoService() throws SQLException {
//        databaseHelper = new DatabaseHelper(context);
//        dao = new ConfiguracaoDao(databaseHelper.getConnectionSource(), Configuracao.class);
//    }

    public Configuracao getConfiguracao() {
        try {
            if (configuracao == null || configuracao.get_id() == 0) {
                List<Configuracao> lista = null;

                lista = configuracaoDao.queryForAll();
                if (lista.size() > 0) {
                    configuracao = lista.get(0);
                } else {
                    configuracao = new Configuracao();
                    configuracaoDao.create(configuracao);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
        this.configuracao.setDataMod(new Date(System.currentTimeMillis()));
        try {
            configuracaoDao.createOrUpdate(this.configuracao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
