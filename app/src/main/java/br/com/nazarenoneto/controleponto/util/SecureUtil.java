package br.com.nazarenoneto.controleponto.util;

import android.content.Context;
import android.provider.Settings;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by nazareno on 16/05/2016.
 */
@EBean
public class SecureUtil {
    @RootContext
    Context context;

    public String getUniqueId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
